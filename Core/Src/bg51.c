/*
 * AMSAT-DL BG51 radiation sensor
 * Service routines for BG51 radiation sensor
 *
 * */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_tim.h"
#include "stm32l4xx_hal_i2c.h"

#include "bg51.h"
#include "sensors.h"
#include "i2c_hal.h"

bg51_radiation_t bg51_value;
static volatile uint16_t bg51_ctr;
static unsigned long t_last = 0;

void bg51_service(void)
{
    unsigned long time;
    unsigned long dt;

    time = timeNowUs();
    if (bg51_ctr >= INTEGRATION_COUNT) {
        dt = time-t_last;
        t_last = time;
        bg51_value.timestamp = time;
        bg51_value.integration_time = dt;  // in [µs]
        bg51_value.counts = bg51_ctr;
        printf("t=%lu; c=%d\n", dt, bg51_ctr);  // FIXME: remove printf
        bg51_ctr = 0;
        sensordata.bg51.timestamp = bg51_value.timestamp;
        sensordata.bg51.integration_time = bg51_value.integration_time;
        sensordata.bg51.counts = bg51_value.counts;
    }
    return;
}

void bg51_callback(uint16_t n)
{
    bg51_ctr++;
    return;
}
