/*
 * I2C-based HALs for SH2.
 */

#include "main.h"

#include "i2c_hal.h"

#include "sh2_hal_init.h"
#include "sh2_hal.h"
#include "sh2_err.h"

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_tim.h"
#include "stm32l4xx_hal_i2c.h"

// Keep reset asserted this long.
// (Some targets have a long RC decay on reset.)
#define RESET_DELAY_US (10000)

// Wait up to this long to see first interrupt from SH
#define START_DELAY_US (4000000) // (2000000)

// Wait this long before assuming bootloader is ready
#define DFU_BOOT_DELAY_US (3000000) // (50000)

// How many bytes to read when reading the length field
#define READ_LEN (2)

enum BusState_e {
    BUS_INIT,
    BUS_IDLE,
    BUS_READING_LEN,
    BUS_GOT_LEN,
    BUS_READING_TRANSFER,
    BUS_WRITING,
    BUS_READING_DFU,
    BUS_WRITING_DFU,
};

static bool isOpen = false;

// Timer handle
TIM_HandleTypeDef tim2;

// I2C Peripheral, I2C2
extern I2C_HandleTypeDef hi2c2;

enum BusState_e i2cBusState;

volatile uint32_t rxTimestamp_uS;            // timestamp of INTN event

// Receive Buffer
static uint8_t rxBuf[SH2_HAL_MAX_TRANSFER_IN];      // data
static uint32_t rxBufLen;   // valid bytes stored in rxBuf (0 when buf empty)
static uint16_t payloadLen;

// Transmit buffer
static uint8_t txBuf[SH2_HAL_MAX_TRANSFER_OUT];

// True after INTN observed, until read starts
static bool rxDataReady;
static uint32_t discards = 0;

// I2C Addr (in 7 MSB positions)
static uint16_t i2cAddr;

// True between asserting reset and seeing first INTN assertion
static volatile bool inReset;

static void enableInts(void)
{
    // Enable INTN interrupt
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

    // Enable I2C interrupts
    HAL_NVIC_EnableIRQ(I2C2_EV_IRQn);
    //HAL_NVIC_EnableIRQ(I2C2_ER_IRQn);
}

static void disableInts(void)
{
    // Disable I2C interrupts
    //HAL_NVIC_DisableIRQ(I2C2_ER_IRQn);
    HAL_NVIC_DisableIRQ(I2C2_EV_IRQn);

    // Disable INTN interrupt line
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
}

static void enableI2cInts(void)
{
    // Enable I2C interrupts
    HAL_NVIC_EnableIRQ(I2C2_EV_IRQn);
    //HAL_NVIC_EnableIRQ(I2C2_ER_IRQn);
}

static void disableI2cInts(void)
{
    // Disable I2C interrupts
    //HAL_NVIC_DisableIRQ(I2C2_ER_IRQn);
    HAL_NVIC_DisableIRQ(I2C2_EV_IRQn);
}

static void hal_init_gpio(void)
{
	/* BNO086 Hardware Init */

	// NRST    => PB13 -> High => Sequenz: low 10ns, wait1 90ms, wait2 4ms
	// CLKSEL0 => PB14 -> Low => External crystal
	// BOOTN   => PB15 -> High

	HAL_GPIO_WritePin(BNO_NBoot_GPIO_Port, BNO_NBoot_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(BNO_ClkSel0_GPIO_Port, BNO_ClkSel0_Pin, GPIO_PIN_RESET);


	HAL_GPIO_WritePin(BNO_NRst_GPIO_Port, BNO_NRst_Pin, GPIO_PIN_SET);
	HAL_Delay(110);
	HAL_Delay(10);
	HAL_GPIO_WritePin(BNO_NRst_GPIO_Port, BNO_NRst_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(BNO_NRst_GPIO_Port, BNO_NRst_Pin, GPIO_PIN_SET);
	HAL_Delay(110);
}

static void hal_init_i2c(void)
{
	HAL_StatusTypeDef result;

	result = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)i2cAddr, 1, 100);
	HAL_Delay(100);
	result = result;  // avoid variabe unused warning
}

static void hal_init_timer(void)
{
	__HAL_RCC_TIM2_CLK_ENABLE();

    // Prescale to get 1 count per uS
    uint32_t prescaler = (uint32_t)((HAL_RCC_GetPCLK2Freq() / 1000000) - 1);

    tim2.Instance = TIM2;
    tim2.Init.Period = 0xFFFFFFFF;
    tim2.Init.Prescaler = prescaler;
    tim2.Init.ClockDivision = 0;
    tim2.Init.CounterMode = TIM_COUNTERMODE_UP;

    HAL_TIM_Base_Init(&tim2);
    HAL_TIM_Base_Start(&tim2);
}

static void hal_init_hw(void)
{
    hal_init_timer();
    hal_init_gpio();
    hal_init_i2c();
}

static void bootn(bool state)
{
    HAL_GPIO_WritePin(BNO_NBoot_GPIO_Port, BNO_NBoot_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

static void rstn(bool state)
{
    HAL_GPIO_WritePin(BNO_NRst_GPIO_Port, BNO_NRst_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

static void ps0_waken(bool state)
{
//    HAL_GPIO_WritePin(PS0_WAKEN_PORT, PS0_WAKEN_PIN,
//                      state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

static void ps1(bool state)
{
//    HAL_GPIO_WritePin(PS1_PORT, PS1_PIN,
//                      state ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

uint32_t timeNowUs(void)
{
    return __HAL_TIM_GET_COUNTER(&tim2);
}

static void delay_us(uint32_t t)
{
    uint32_t now = timeNowUs();
    uint32_t start = now;
    while ((now - start) < t)
    {
        now = timeNowUs();
    }
}

static void reset_delay_us(uint32_t t)
{
    uint32_t now = timeNowUs();
    uint32_t start = now;
    while (((now - start) < t) && (inReset))
    {
        now = timeNowUs();
    }
}

// ----------------------------------------------------------------------------------
// Callbacks for ISR, I2C Operations
// ----------------------------------------------------------------------------------

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *pI2c)
{
    // Read completed
    if (i2cBusState == BUS_READING_LEN)
    {
        // Len of payload is available, decide how long to do next read
        uint16_t len = (rxBuf[0] + (rxBuf[1] << 8)) & ~0x8000;
        if (len > sizeof(rxBuf))
        {
            // read only what will fit in rxBuf
            payloadLen = sizeof(rxBuf);
        }
        else
        {
            payloadLen = len;
        }
        i2cBusState = BUS_GOT_LEN;
    }
    else if (i2cBusState == BUS_READING_TRANSFER)
    {
        // rxBuf is now ready for client.
        rxBufLen = payloadLen;

        // Nothing left to do
        i2cBusState = BUS_IDLE;
    }
    else if (i2cBusState == BUS_READING_DFU)
    {
        // Transition back to idle state
        rxBufLen = payloadLen;
        i2cBusState = BUS_IDLE;
    }
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *i2c)
{
    if (i2cBusState == BUS_WRITING)
    {
        // Switch back to bus idle
        i2cBusState = BUS_IDLE;
    }
    else if (i2cBusState == BUS_WRITING_DFU)
    {
        // Switch back to bus idle
        i2cBusState = BUS_IDLE;
    }
}

void bno_callback(uint16_t n)
{
    if (i2cBusState == BUS_INIT)
    {
        // No active hal, ignore this call, don't crash.
        return;
    }
    
    rxTimestamp_uS = timeNowUs();
    inReset = false;

    // Start read, if possible
    if (i2cBusState == BUS_IDLE)
    {
        if (rxBufLen > 0)
        {
            // Discard earlier payload!
            discards++;
            rxBufLen = 0;
        }
        
        // Read payload len
        i2cBusState = BUS_READING_LEN;
        HAL_I2C_Master_Receive_IT(&hi2c2, i2cAddr, rxBuf, READ_LEN);
    }
    else if (i2cBusState == BUS_GOT_LEN)
    {
        // Read payload
        i2cBusState = BUS_READING_TRANSFER;
        HAL_I2C_Master_Receive_IT(&hi2c2, i2cAddr, rxBuf, payloadLen);
    }
    else
    {
        // We can't start read immediately, set flag so it gets done later.
        rxDataReady = true;
    }
}

// ------------------------------------------------------------------------
// SH2 HAL Methods

static int shtp_i2c_hal_open(sh2_Hal_t *self_)
{
    // cast sh2_hal_t pointer to i2c_hal_t
    i2c_hal_t *self = (i2c_hal_t *)self_;
    
    if (isOpen)
    {
        return SH2_ERR;
    }

    i2cBusState = BUS_INIT;
    i2cAddr = self->i2c_addr << 1;

    isOpen = true;

    // Init hardware peripherals
    hal_init_hw();

    // Hold in reset, not for DFU
    rstn(false);

    inReset = true;  // will change back to false when INTN serviced

    enableInts();

    // Delay for RESET_DELAY_US to ensure reset takes effect
    delay_us(RESET_DELAY_US);
    
    // transition to idle state
    i2cBusState = BUS_IDLE;

    // Clear rx, tx buffers
    rxBufLen = 0;
    rxDataReady = false;

    // To boot in SHTP-I2C mode, must have PS1=0, PS0=0.
    // PS1 is set via jumper.
    // PS0 will be 0 if PS0 jumper is 0 OR (PS1 jumper is 1 AND PS0_WAKEN sig is 0)
    // So we set PS0_WAKEN signal to 0 just in case PS1 jumper is in 1 position.
    ps0_waken(false);
    ps1(false);

    // Deassert BOOT, don't go into bootloader
    bootn(!self->dfu);
    
    // Deassert reset
    rstn(1);

    // Wait for INTN to be asserted
    reset_delay_us(START_DELAY_US);

    return SH2_OK;
}

static void shtp_i2c_hal_close(sh2_Hal_t *self_)
{
    // Hold sensor hub in reset
    rstn(false);
    bootn(true);

    i2cBusState = BUS_INIT;

    // Disable interrupts
    disableInts();
    
    // Deinit I2C peripheral
//    HAL_I2C_DeInit(&i2c);
    
    // Deinit timer
//    __HAL_TIM_DISABLE(&tim2);
    
    isOpen = false;
}

static int shtp_i2c_hal_read(sh2_Hal_t *self_, uint8_t *pBuffer, unsigned len, uint32_t *t)
{
    int retval = 0;
    
    disableInts();
    if (rxBufLen > 0)
    {
        // There is data to be had.
        if (len < rxBufLen)
        {
            // Client buffer too small!
            // Discard what was read
            rxBufLen = 0;
            retval = SH2_ERR_BAD_PARAM;
        }
        else
        {
            // Copy data to the client buffer
            memcpy(pBuffer, rxBuf, rxBufLen);
            retval = rxBufLen;
            rxBufLen = 0;
            *t = rxTimestamp_uS;
        }
    }
    enableInts();

    // if more data is ready, start reading it
    if (rxDataReady)
    {
        if ((i2cBusState == BUS_IDLE))
        {
            rxDataReady = false;
            i2cBusState = BUS_READING_LEN;
            HAL_I2C_Master_Receive_IT(&hi2c2, i2cAddr, rxBuf, READ_LEN);
        }
        else if ((i2cBusState == BUS_GOT_LEN))
        {
            rxDataReady = false;
            i2cBusState = BUS_READING_TRANSFER;
            HAL_I2C_Master_Receive_IT(&hi2c2, i2cAddr, rxBuf, payloadLen);
        }
    }

    return retval;
}

static int shtp_i2c_hal_write(sh2_Hal_t *self, uint8_t *pBuffer, unsigned len)
{
    int retval = 0;
    
    // Validate parameters
    if ((pBuffer == 0) || (len == 0) || (len > sizeof(txBuf)))
    {
        return SH2_ERR_BAD_PARAM;
    }

    // Disable I2C Interrupt for a moment so busState can't change
    disableI2cInts();
    
    if (i2cBusState == BUS_IDLE)
    {
        i2cBusState = BUS_WRITING;

        // Set up write operation
        memcpy(txBuf, pBuffer, len);
        HAL_I2C_Master_Transmit_IT(&hi2c2, i2cAddr, txBuf, len);

        retval = len;
    }

    // re-enable interrupts
    enableI2cInts();
    
    return retval;
}

static uint32_t shtp_i2c_hal_getTimeUs(sh2_Hal_t *self)
{
    return timeNowUs();
}

sh2_Hal_t *shtp_i2c_hal_init(i2c_hal_t *pHal, bool dfu, uint8_t addr)
{
    pHal->dfu = dfu;
    pHal->i2c_addr = addr;

    pHal->sh2_hal.open = shtp_i2c_hal_open;
    pHal->sh2_hal.close = shtp_i2c_hal_close;
    pHal->sh2_hal.read = shtp_i2c_hal_read;
    pHal->sh2_hal.write = shtp_i2c_hal_write;
    pHal->sh2_hal.getTimeUs = shtp_i2c_hal_getTimeUs;

    return &pHal->sh2_hal;
}
