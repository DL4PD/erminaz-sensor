/*
 * AMSAT-DL Sensor Board application
 * */

#include <stdio.h>
#include <string.h>

#include "main.h"

#include "sensors.h"
#include "i2c_hal.h"

#include "bg51.h"

#include "sh2.h"
#include "sh2_util.h"
#include "sh2_err.h"
#include "sh2_SensorValue.h"
#include "sh2_hal_init.h"

static void delayUs(uint32_t t);
static void data(const sh2_SensorEvent_t * event);

erminaz_sensordata_t sensordata;
erminaz_sensordata_t sensordata_buf[DATA_BUF_SIZE];

sh2_ProductIds_t prodIds;
sh2_Hal_t *pSh2Hal = 0;

bool resetOccurred = false;

void HAL_GPIO_EXTI_Callback(uint16_t n)
{
    switch(n) {
    case BG51_Int_Pin:	 /* BG51 */
        bg51_callback(n);
        break;
    case BNO_IntN_Pin: /* BNO086*/
        bno_callback(n);
        break;
    }
}

static void writebuffer(erminaz_sensordata_t pBuf[], erminaz_sensordata_t pData)
{
    static int wp = 0;

    pBuf[wp] = pData;
    if (++wp >= DATA_BUF_SIZE) {
        wp = 0;
    }
    return;
}

/*
 * Configure sensors
 * */
static void startReports()
{
    static sh2_SensorConfig_t config;
    int status;
    int sensorId;
    static const int enabledSensors[] =
    {
            SH2_TEMPERATURE,
            SH2_HUMIDITY,
            SH2_PRESSURE,
            SH2_ACCELEROMETER,
            SH2_RAW_GYROSCOPE,
            SH2_ROTATION_VECTOR,
            SH2_GYRO_INTEGRATED_RV,
            SH2_MAGNETIC_FIELD_CALIBRATED,
    };

    // These sensor options are disabled or not used in most cases
    config.changeSensitivityEnabled = false;
    config.wakeupEnabled = false;
    config.changeSensitivityRelative = false;
    config.alwaysOnEnabled = false;
    config.sniffEnabled = false;
    config.changeSensitivity = 0;
    config.batchInterval_us = 0;
    config.sensorSpecific = 0;

    // Select a report interval.
    config.reportInterval_us = 1000000;  // microseconds (1 Hz)
    // config.reportInterval_us = 100000;  // microseconds (10 Hz)
    // config.reportInterval_us = 40000;  // microseconds (25 Hz)
    //config.reportInterval_us = 10000;  // microseconds (100 Hz)
    // config.reportInterval_us = 2500;   // microseconds (400 Hz)
    // config.reportInterval_us = 1000;   // microseconds (1000 Hz)

    for (int n = 0; n < ARRAY_LEN(enabledSensors); n++)
    {
        // Configure the sensor hub to produce these reports
        sensorId = enabledSensors[n];
        status = sh2_setSensorConfig(sensorId, &config);
        if (status != 0) {
            printf("Error while enabling sensor %d\n", sensorId);
            // to do: set error code
        }
    }
}

// Handle non-sensor events from the sensor hub
static void eventHandler(void * cookie, sh2_AsyncEvent_t *pEvent)
{
    // If we see a reset, set a flag so that sensors will be reconfigured.
    if (pEvent->eventId == SH2_RESET) {
        resetOccurred = true;
    }
}

/*
 * Copy sensor data into global structure
 * */
static void data(const sh2_SensorEvent_t * event)
{
    sh2_SensorValue_t value;

    // Convert event to value
    sh2_decodeSensorEvent(&value, event);

    /*
     * FIXME:   the sensor data is arriving asynchronously, but we need
     *          a periodic update in the circular buffer. Evaluate a
     *          method to update the circular buffer once a second.
     * */
    //memcpy(&sensor_buf[0], &value, sizeof(sh2_SensorValue_t));  // copy data into circular buffer

    switch (value.sensorId) {
    case SH2_MAGNETIC_FIELD_CALIBRATED:
        sensordata.mag.hdr.delay = value.delay;
        sensordata.mag.hdr.sensorId = value.sensorId;
        sensordata.mag.hdr.sequence = value.sequence;
        sensordata.mag.hdr.status = value.status;
        sensordata.mag.hdr.timestamp = value.timestamp;
        sensordata.mag.hdr.status = value.status & 0x3;
        sensordata.mag.magneticField.x = value.un.magneticField.x;
        sensordata.mag.magneticField.y = value.un.magneticField.y;
        sensordata.mag.magneticField.z = value.un.magneticField.z;
        break;

    case SH2_ACCELEROMETER:
        sensordata.acc.hdr.delay = value.delay;
        sensordata.acc.hdr.sensorId = value.sensorId;
        sensordata.acc.hdr.sequence = value.sequence;
        sensordata.acc.hdr.status = value.status;
        sensordata.acc.hdr.timestamp = value.timestamp;
        sensordata.acc.accelerometer.x = value.un.accelerometer.x;
        sensordata.acc.accelerometer.y = value.un.accelerometer.y;
        sensordata.acc.accelerometer.z = value.un.accelerometer.z;
        break;

    case SH2_ROTATION_VECTOR:
        sensordata.rot.hdr.delay = value.delay;
        sensordata.rot.hdr.sensorId = value.sensorId;
        sensordata.rot.hdr.sequence = value.sequence;
        sensordata.rot.hdr.status = value.status;
        sensordata.rot.hdr.timestamp = value.timestamp;
        sensordata.rot.rotationVector.real = value.un.rotationVector.real;
        sensordata.rot.rotationVector.i = value.un.rotationVector.i;
        sensordata.rot.rotationVector.j = value.un.rotationVector.j;
        sensordata.rot.rotationVector.k = value.un.rotationVector.k;
        sensordata.rot.rotationVector.accuracy = value.un.rotationVector.accuracy;
        break;

    case SH2_RAW_GYROSCOPE:
        sensordata.rgy.hdr.delay = value.delay;
        sensordata.rgy.hdr.sensorId = value.sensorId;
        sensordata.rgy.hdr.sequence = value.sequence;
        sensordata.rgy.hdr.status = value.status;
        sensordata.rgy.hdr.timestamp = value.timestamp;
        sensordata.rgy.rawGyroscope.x = value.un.rawGyroscope.x;
        sensordata.rgy.rawGyroscope.y = value.un.rawGyroscope.y;
        sensordata.rgy.rawGyroscope.z = value.un.rawGyroscope.z;
        break;

    case SH2_GYRO_INTEGRATED_RV:
        sensordata.grs.hdr.delay = value.delay;
        sensordata.grs.hdr.sensorId = value.sensorId;
        sensordata.grs.hdr.sequence = value.sequence;
        sensordata.grs.hdr.status = value.status;
        sensordata.grs.hdr.timestamp = value.timestamp;
        sensordata.grs.gyroIntegratedRV.angVelX = value.un.gyroIntegratedRV.angVelX;
        sensordata.grs.gyroIntegratedRV.angVelY = value.un.gyroIntegratedRV.angVelY;
        sensordata.grs.gyroIntegratedRV.angVelZ = value.un.gyroIntegratedRV.angVelZ;
        sensordata.grs.gyroIntegratedRV.real = value.un.gyroIntegratedRV.real;
        sensordata.grs.gyroIntegratedRV.i = value.un.gyroIntegratedRV.i;
        sensordata.grs.gyroIntegratedRV.j = value.un.gyroIntegratedRV.j;
        sensordata.grs.gyroIntegratedRV.k = value.un.gyroIntegratedRV.k;
        break;

    case SH2_TEMPERATURE:
        sensordata.tmp.hdr.delay = value.delay;
        sensordata.tmp.hdr.sensorId = value.sensorId;
        sensordata.tmp.hdr.sequence = value.sequence;
        sensordata.tmp.hdr.status = value.status;
        sensordata.tmp.hdr.timestamp = value.timestamp;
        sensordata.tmp.temperature.value = value.un.temperature.value;
        break;

    case SH2_PRESSURE:
        sensordata.prs.hdr.delay = value.delay;
        sensordata.prs.hdr.sensorId = value.sensorId;
        sensordata.prs.hdr.sequence = value.sequence;
        sensordata.prs.hdr.status = value.status;
        sensordata.prs.hdr.timestamp = value.timestamp;
        sensordata.prs.pressure.value = value.un.pressure.value;
        break;

    case SH2_HUMIDITY:
        sensordata.hum.hdr.delay = value.delay;
        sensordata.hum.hdr.sensorId = value.sensorId;
        sensordata.hum.hdr.sequence = value.sequence;
        sensordata.hum.hdr.status = value.status;
        sensordata.hum.hdr.timestamp = value.timestamp;
        sensordata.hum.humidity.value = value.un.humidity.value;
        break;

    default:
        break;
    }
}

static void delayUs(uint32_t t)
{
    uint32_t now_us = pSh2Hal->getTimeUs(pSh2Hal);
    uint32_t start_us = now_us;

    while (t > (now_us - start_us))
    {
        now_us = pSh2Hal->getTimeUs(pSh2Hal);
    }
}

// Handle sensor events.
static void sensorHandler(void * cookie, sh2_SensorEvent_t *pEvent)
{
    /*
     * Add data to telemetry dataset
     * */
    data(pEvent);
}

// --- Public methods -------------------------------------------------

// Initialize demo. 
void sensors_init(void)
{
    int status;

    // Create HAL instance
    pSh2Hal = sh2_hal_init();

    // Open SH2 interface (also registers non-sensor event handler.)
    status = sh2_open(pSh2Hal, eventHandler, NULL);
    if (status != SH2_OK) {
        // todo: error code
        printf("Error, %d, from sh2_open.\n", status);
    }

    // Register sensor listener
    sh2_setSensorCallback(sensorHandler, NULL);

    // resetOccurred would have been set earlier.
    // We can reset it since we are starting the sensor reports now.
    resetOccurred = false;

    // Start the flow of sensor reports
    startReports();
}

// This must be called periodically.  (The demo main calls it continuously in a loop.)
// It calls sh2_service to keep data flowing between host and sensor hub.
void sensors_service(void)
{
    static uint64_t last = 0, dt = 0, time = 0;

    time = timeNowUs();
    dt = time - last;

    if (resetOccurred) {
        // Restart the flow of sensor reports
        resetOccurred = false;
        startReports();
    }

    // Service the sensor hub.
    // Sensor reports and event processing handled by callbacks.
    sh2_service();
    bg51_service();

    // update circular buffer approximately every 1s
    if(dt >= 1000000) {
        last = time;
        dt = 0;
        sensordata.timestamp = time;
        writebuffer(sensordata_buf, sensordata);
    }
}
